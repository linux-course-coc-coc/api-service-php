<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Http\Controllers\ExampleController;
use Tests\TestCase;

/**
 * Class ExampleControllerTest
 * @package Tests\Unit
 */
class ExampleControllerTest extends TestCase
{
    /**
     * @test
     * @return void
     */
    public function Success_When_DataInvalid_Expect_ResponseCorrect()
    {
        $example = new ExampleController(false, 400);

        $this->assertEquals(response([
            'success' => false,
            'code' => 400,
        ], 400), $example->success());
    }

    /**
     * @test
     * @return void
     */
    public function Success_When_DataInvalid_Expect_ResponseIncorrect()
    {
        $example = new ExampleController(false, 400);

        $this->assertNotEquals(response([
            'success' => true,
            'code' => 200,
        ]), $example->success());
    }

    /**
     * @test
     * @return void
     */
    public function Error_When_DataInvalid_Expect_ResponseCorrect()
    {
        $example = new ExampleController(false, 400);

        $this->assertEquals(response([
            'success' => false,
            'code' => 400,
        ], 400), $example->error());
    }

    /**
     * @test
     * @return void
     */
    public function Error_When_DataInvalid_Expect_ResponseIncorrect()
    {
        $example = new ExampleController(false, 400);

        $this->assertNotEquals(response([
            'success' => true,
            'code' => 200,
        ], 400), $example->error());
    }

    // /**
    //  * @test
    //  * @return void
    //  */
    // public function Error_When_DataInvalid_Expect_Fail()
    // {
    //     $example = new ExampleController(false, 400);
    //
    //     $this->assertEquals(response([
    //         'success' => false,
    //         'code' => 400,
    //     ], 200), $example->error());
    // }
}
