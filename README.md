# API PHP Service

Project for Coc Coc Linux Course

### Start Project

> **NOTE:** Requires a machine with `docker` and `docker-compose` installed.

#### 1. Clone `.env.example` to `.env`
```bash
cp .env.example .env
```

#### 2. Build and run docker

```bash
docker-compose build --no-cache && docker-compose up
```

#### 3. Install composer

```bash
docker-compose exec php /bin/sh

composer install
```

#### 4. Chmod storage/

```bash
docker-compose exec php /bin/sh

chmod -R 777 storage/
```

### API Endpoint
#### Example API

When call API `http://localhost:8080/api/example`

```bash
curl --request GET --url http://localhost:8080/api/example
```
Response:

```json
{
  "success": true,
  "code": 200
}
```

Change the response by editing the file `routes/api.php`. Comment *line 23* and uncomment *line 26* like below.

```php
// Route example api for success
// Route::get('/example', [ExampleController::class, 'success']);

// Route example api for error
Route::get('/example', [ExampleController::class, 'error']);
```

Try calling the API again and the result should look like this:

```json
{
  "success": false,
  "code": 400
}
```

### Calculate API

When call API:

`http://localhost:8080/api/calculate?a=102345678&operator=*&b=987654`

```bash
curl --request GET --url 'http://localhost:8080/api/calculate?a=102345678&operator=*&b=987654'
```

Response:

```json
{
  "success": true,
  "amount": 101082118259412
}
```
Parameters:

`a` : Number a for calculate

`operator`: Operator for calculate. Include `+`, `-`, `*`, `/`

`b` : Number b for calculate

### Error Case

When passing parameters `a` and `b` as strings or using division with `b` as `0`, an error will occur.

Example:

```bash
curl --request GET \
  --url 'http://localhost:8080/api/calculate?a=csdfds&operator=%2B&b=fdsgds'
```

```bash
curl --request GET \
  --url 'http://localhost:8080/api/calculate?a=523532666&operator=%2F&b=0'
```

To fix this, you need to edit the file `app/Http/Controllers/CalculateController.php`. 
Uncomment `line 28`

```php
$this->validateRequest($request);

$this->validateDivisionByZero($request);

$a = $request->a;
$b = $request->b;
```

And edit `validateRequest` function in `line 48` to

```php
private function validateRequest(Request $request): void
{
    // $request->validate([
    //    'a' => 'required',
    //    'operator' => 'nullable|in:+,-,*,/',
    //    'b' => 'required',
    // ]);

    $request->validate([
        'a' => 'required|numeric',
        'operator' => 'nullable|in:+,-,*,/',
        'b' => 'required|numeric',
    ]);
}
```

### Unit Test

To run Unit Test for this repository, run the following commands in PHP Container

```bash
docker-compose exec php /bin/sh

./vendor/bin/phpunit --testdox
```

Response:

```
/var/www/html $ ./vendor/bin/phpunit --testdox
PHPUnit 10.2.2 by Sebastian Bergmann and contributors.

Runtime:       PHP 8.2.7
Configuration: /var/www/html/phpunit.xml

.....                                                               5 / 5 (100%)

Time: 00:00.120, Memory: 24.00 MB

Example (Tests\Feature\Example)
 ✔ The application returns a successful response

Example Controller (Tests\Unit\ExampleController)
 ✔ Success When DataInvalid Expect ResponseCorrect
 ✔ Success When DataInvalid Expect ResponseIncorrect
 ✔ Error When DataInvalid Expect ResponseCorrect
 ✔ Error When DataInvalid Expect ResponseIncorrect
```

### Check Coding Convention

Run this command in PHP Container

```bash
docker-compose exec php /bin/sh

./vendor/bin/phpcs
```

If there is no syntax error, you will see it return like this

```bash
/var/www/html $ ./vendor/bin/phpcs
..................... 21 / 21 (100%)


Time: 42ms; Memory: 8MB
```

### Check Copy/Paste Detector (CPD)

In /bin/sh PHP Docker run this command

```bash
 php phpcpd.phar --fuzzy app/
 php phpcpd.phar --fuzzy src/
```

Response:

```
/var/www/html $ php phpcpd.phar --fuzzy src/
phpcpd 6.0.3 by Sebastian Bergmann.

No files found to scan
```

### Making Gitlab CI Error

To make Gitlab CI process fail. Uncomment function `Error_When_DataInvalid_Expect_Fail` (`line 76`) in file `tests/Unit/ExampleControllerTest.php`

```php
/**
 * @test
 * @return void
 */
public function Error_When_DataInvalid_Expect_Fail()
{
    $example = new ExampleController(false, 400);

    $this->assertEquals(response([
        'success' => false,
        'code' => 400,
    ], 200), $example->error());
}
```

Then `commit` and `push` the code on the repository.
