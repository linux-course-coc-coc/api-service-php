import requests
import json

URL = "http://127.0.0.1:8080/api/calculate?a=$a&operator=$operator&b=$b"

TEST_CASE = [
    (5843, '*', 5548, True, 32416964),
    (8433, '/', 3491, True, 2.415640217702664),
    (3403, '-', 1110, True, 2293),
    (643, '/', 9737, True, 0.06603676697134642),
    (1807, '/', 2789, True, 0.6479024740050198),
    (1694, '+', 4799, True, 6493),
    (316, '/', 275, True, 1.1490909090909092),
    (7740, '-', 4977, True, 2763),
    (2337, '/', 3544, True, 0.6594243792325056),
    (6363, '-', 8344, True, -1981),
    (527, '*', 3913, True, 2062151),
    (6284, '*', 8234, True, 51742456),
    (1528, '-', 9012, True, -7484),
    (3254, '/', 8394, True, 0.38765785084584226),
    (3095, '-', 4453, True, -1358),
    (4178, '-', 8451, True, -4273),
    (5689, '*', 7560, True, 43008840),
    (9009, '+', 5927, True, 14936),
    (6413, '+', 5063, True, 11476),
    (7436, '/', 3525, True, 2.109503546099291),
    (1, '/', 0, False, -1),
    ('x', '*', 'y', False, -1)
]


def getURL(a: str, operator: str, b: str):
    return URL.replace("$a", a).replace("$operator", operator).replace("$b", b)

for test_case in TEST_CASE:
    print(test_case)
    (a, operator, b, status, expect) = test_case
    url = getURL(str(a), operator, str(b))
    resp = requests.get(url=url, timeout=1)
    content = resp.content.decode(encoding='utf-8')
    r = json.loads(content)
    is_success = r["success"]
    amount = -1
    if "amount" in r.keys():
        amount = float(r["amount"])
    
    assert status == is_success, (test_case, content)
    if isinstance(expect, int):
        assert expect == amount, (test_case, content)
    else:
        assert abs(expect - amount) / abs(expect) < 0.001, (test_case, content)

print("Test result: PASS!!!")
